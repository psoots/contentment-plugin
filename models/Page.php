<?php namespace Castiron\Contentment\Models;

use Castiron\Peaches\Support\Arr;
use Model;
use Str;
use Lang;
use Cms\Classes\Theme;
use Cms\Classes\Layout;
use Cms\Classes\Page as CmsPage;
use Castiron\Contentment\Content\Manager as ContentManager;

/**
 * Page Model
 */
class Page extends Model
{

    use \October\Rain\Database\Traits\Sluggable;
    use \October\Rain\Database\Traits\SoftDeleting;
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\NestedTree;
    use \Castiron\Contentment\Traits\Aliasable;
    use \Castiron\Contentment\Traits\SemanticUrlable;

    const TYPE_CONTENT = 0;
    const TYPE_REDIRECT = 1;

    public $rules = [
        'title' => 'required',
        'slug' => 'required',
        'reference' => 'unique:castiron_contentment_pages,reference',
        'template' => 'required',
    ];

    /**
     * @var string Field used by the aliasable trait
     */
    protected $aliasField = 'reference';

    /**
     * @var string Field used by the semanticurlable trait for path segment value
     */
    protected $segmentField = 'slug';

    /**
     * @var string Field used by the semanticurlable trait for the field that refers to the parent
     */
    protected $parentIdField = 'parent_id';

    /**
     * @var string The database table used by the model.
     */
    public $table = 'castiron_contentment_pages';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['title','slug','template','navigation_title','parent_id','is_hidden','reference', 'type', 'redirect_url'];

    protected $slugs = [
        'slug' => 'title'
    ];

    protected $jsonable = [
        'content'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'contents' => [
            Content::class
        ]
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getTypeOptions()
    {
        return [
            self::TYPE_CONTENT => 'Content Page',
            self::TYPE_REDIRECT => 'Redirect Page',
        ];
    }

    public function scopeVisible($query)
    {
        return $query->where(function($query) {
            $query->where('is_hidden', '<>', 1)->orWhereNull('is_hidden');
        })->where(function($query) {
            $query->where('startdate', '<=', \Carbon\Carbon::now())->orWhereNull('startdate');
        })->where(function($query) {
            $query->where('enddate', '>=', \Carbon\Carbon::now())->orWhereNull('enddate');
        });
    }


    public function getVisibleChildren()
    {
        return $this->children()->visible()->get();
    }

    /**
     * If content blocks don't have a section_title, then we don't render it in the menu.
     *
     * @param bool $anchorableOnly
     * @deprecated
     * @return array
     */
    public function getContentHeaders($anchorableOnly = true)
    {
        $headers = [];
        $content = $this->content;
        if (!isset($content) || !is_array($content)) return [];

        foreach ($content as $block) {
            if ($anchorableOnly && isset($block['section_not_anchor']) && $block['section_not_anchor']) {
                continue;
            }
            if (trim($block['section_title'])) {
                $headers[] = $block['section_title'];
            }
        }

        return $headers;
    }

    /**
     * @param string $glue
     * @return string
     */
    public function renderContent($glue = "\n")
    {
        $items = $this->contents()->ordered()->visible()->get()->all();
        return implode($glue, Arr::mapMethod($items, 'render'));
    }


    /**
     * Get all the "elements" by the type. This returns
     * the pseudo model, Element. Not the actual Content model.
     *
     * @param string|array $elementIdentifiers Like "Castiron.Moca.Section" to get the section elements
     * @return array
     */
    public function elementsByTypes($elementIdentifiers)
    {
        if (!is_array($elementIdentifiers)) {
            $elementIdentifiers = func_get_args();
        }
        $types = [];
        foreach ($elementIdentifiers as $id) {
            $elementInfo = ContentManager::instance()->elements($id);
            $types[] = $elementInfo->class;
        }
        $contents = $this->contents()->ordered()->visible()->whereIn('element_type', $types)->get()->all();
        return Arr::mapMethod($contents, 'element');
    }

    /**
     * @param string $elementIdentifier Like "Castiron.Moca.Section" to get the section elements
     * @return array
     */
    public function elementsByType($elementIdentifier)
    {
        return $this->elementsByTypes($elementIdentifier);
    }

    /**
     * Do some validation of content blocks before rendering them.
     *
     * @return array
     * @deprecated
     */
    public function getValidContentBlocks()
    {
        $blocks = [];
        $content = $this->content;
        if (!isset($content) || !is_array($content)) return [];
        foreach ($content as $block) {

            $block['no_content'] = false;

            if ($block['type'] == 'richeditor') {
                // Let's make sure our content doesn't just consist of empty "<p></p>" tags.
                $val = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', strip_tags($block['section_content']));
                if (!$val) {
                    $block['no_content'] = true;
                }
            }

            $blocks[] = $block;
        }
        return $blocks;
    }

    public static function findByIdentifier($identifier)
    {
        $page = self::find($identifier);
        if(!$page) {
            $page = self::findByUrl($identifier);
        }

        return $page;
    }

    public function getTemplateOptions()
    {
        if (!($theme = Theme::getEditTheme())) {
            throw new ApplicationException(Lang::get('cms::lang.theme.edit.not_found'));
        }

        $pages = CmsPage::listInTheme($theme, true);
        $result = [];
        foreach ($pages as $page) {
            if(!array_key_exists('is_content_page', $page['settings']) || !$page['settings']['is_content_page']) continue;
            $baseName = $page->getBaseFileName();
            $result[$baseName] = $page->title;
        }

        return $result;
    }

    public static function extendCmsPageForm($formWidget)
    {
        $fieldConfig = [
            'tab' => 'cms::lang.editor.settings',
            'type' => 'checkbox',
            'label' => 'Is Content Page Template',
            'comment' => 'Check this to make this page available as a template for content editors.'

        ];
        $formWidget->tabs['fields']['settings[is_content_page]'] = $fieldConfig;
    }
}
