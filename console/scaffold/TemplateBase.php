<?php

namespace Castiron\Contentment\Console\Scaffold;

use \October\Rain\Scaffold\TemplateBase as Base;

class TemplateBase extends Base
{

    public function makeStub($stubName)
    {
        if (!isset($this->fileMap[$stubName]))
            return;


        // ONLY THING DIFFERENT THAN PARENT
        $sourceFile = __DIR__ . '/templates/' . $stubName;
        // END DIFFERENCE


        $destinationFile = $this->targetPath . '/' . $this->fileMap[$stubName];
        $destinationContent = $this->files->get($sourceFile);

        /*
         * Parse each variable in to the desintation content and path
         */
        foreach ($this->vars as $key => $var) {
            $destinationContent = str_replace('{{'.$key.'}}', $var, $destinationContent);
            $destinationFile = str_replace('{{'.$key.'}}', $var, $destinationFile);
        }

        /*
         * Destination directory must exist
         */
        $destinationDirectory = dirname($destinationFile);
        if (!$this->files->exists($destinationDirectory))
            $this->files->makeDirectory($destinationDirectory, 0777, true); // @todo 777 not supported everywhere

        /*
         * Make sure this file does not already exist
         */
        if ($this->files->exists($destinationFile) && !$this->overwriteFiles)
            throw new \Exception('Stop everything!!! This file already exists: ' . $destinationFile);

        $this->files->put($destinationFile, $destinationContent);
    }
}
