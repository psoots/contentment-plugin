<?php

namespace Castiron\Contentment\Console\Scaffold;

use Castiron\Contentment\Console\Scaffold\Templates\Element;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class CreateElement extends Command
{

    protected $name = 'create:element';

    protected $description = "Creates a new Content Element";

    public function fire()
    {
        /*
         * Extract the author and name from the plugin code
         */
        $pluginCode = $this->argument('pluginCode');
        $parts = explode('.', $pluginCode);
        $pluginName = array_pop($parts);
        $authorName = array_pop($parts);

        $destinationPath = base_path() . '/plugins/' . strtolower($authorName) . '/' . strtolower($pluginName);
        $modelName = $this->argument('elementName');
        $vars = [
            'name' => $modelName,
            'author' => $authorName,
            'plugin' => $pluginName
        ];

        Element::make($destinationPath, $vars, $this->option('force'));

        $code = <<<CODE
use Castiron\Contentment\Content\Manager as ContentManager;

ContentManager::registerElement({$modelName}::class, [
    'icon' => 'icon-something',
    'label' => 'Some Label'
]);
CODE;

        $this->info(sprintf('Successfully generated Element named "%s"', $modelName));
        $this->info('');
        $this->comment("Don't forget to add something like this to your Plugin.php file.");
        $this->info('');
        $this->comment($code);

    }

    protected function getArguments()
    {
        return [
            ['pluginCode', InputArgument::REQUIRED, 'The name of the plugin. Eg: RainLab.Blog'],
            ['elementName', InputArgument::REQUIRED, 'The name of the element. Eg: OrderedList'],
        ];
    }

    protected function getOptions()
    {
        return [
            ['force', null, InputOption::VALUE_NONE, 'Overwrite existing files with generated ones.'],
        ];
    }
}
