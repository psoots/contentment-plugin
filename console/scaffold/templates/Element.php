<?php

namespace Castiron\Contentment\Console\Scaffold\Templates;

use Castiron\Contentment\Console\Scaffold\TemplateBase;

class Element extends TemplateBase
{

    protected $fileMap = [
        'element/element.stub' => 'content/{{studly_name}}.php',
        'element/default.stub' => 'content/{{lower_name}}/default.htm',
        'element/preview.stub' => 'content/{{lower_name}}/preview.htm',
        'element/fields.stub'  => 'content/{{lower_name}}/fields.yaml',
    ];
}
