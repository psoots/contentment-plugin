<?php namespace Castiron\Contentment\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Pages Back-end Controller
 */
class Pages extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Owl.Behaviors.ListDelete.Behavior',
        'Backend.Behaviors.ImportExportController',
        'Backend.Behaviors.ReorderController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $importExportConfig = 'config_import_export.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = ['castiron.contentment.access_pages'];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Castiron.Contentment', 'pages', 'pages');
    }

    public function getModel()
    {
        if ($this->action != 'create') {
            $formWidget = $this->formGetWidget();
            if ($formWidget) {
                return $formWidget->model;
            }
        }
    }

    public function getPageUrl()
    {
        $model = $this->getModel();
        if ($model) {
            return $model->getUrl();
        }
    }

    public function renderBreadcrumb()
    {
        $model = $this->getModel();

        return $this->makePartial('breadcrumb', [
            'pageUrl' => $model->getUrl(),
            'title' => $model->title,
        ]);
    }
}
